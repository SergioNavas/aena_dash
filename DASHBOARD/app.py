import dash
import dash_core_components as dcc
import dash_html_components as html
import base64
import plotly.graph_objects as go
import dash_bootstrap_components as dbc
import dash.dependencies
from dash.dependencies import Input, Output
import datetime
import plotly
import os
import flask
import dash_player
from flask import Flask, Response
import time

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

image_filename_1 = '/Users/sergionavasgarcia/IOON/AENA/DASHBOARD/ioon_logo.jpg'
image_filename_2 = '/Users/sergionavasgarcia/IOON/AENA/DASHBOARD/Turnaround_AENA.mp4'
encoded_image_1 = base64.b64encode(open(image_filename_1, 'rb').read())
encoded_image_2 = base64.b64encode(open(image_filename_2, 'rb').read())

n_abs = 0
init = 1

colors = {
    'background': '#9A9AA0',
    'text': '#7FDBFF',
    'text_hola': '#9A9AA0',
    'plot_title': '#3E3E41'
}

figure = dict({
    'data': [
        {'x': [0], 'y': ['Avion parado'], 'type': 'bar', 'name': 'Avion parado', 'orientation': 'h', 'base': 5, 'width': 0.1},
        {'x': [0], 'y': ['Desembarco'], 'type': 'bar', 'name': 'Desembarco', 'orientation': 'h', 'base': 2, 'width': 0.1},
        {'x': [0], 'y': ['Maletas'], 'type': 'bar', 'name': 'Maletas', 'orientation': 'h', 'width': 0.1},
        {'x': [0], 'y': ['Repostaje'], 'type': 'bar', 'name': 'Repostaje', 'orientation': 'h', 'width': 0.1},
    ],
    'layout': {
        'title': 'Turnaorund events',
        'plot_bgcolor': colors['background'],
        'paper_bgcolor': colors['background'],
        'font':{
            'color': colors['plot_title'],
        }

    }
})

fig = go.Figure(figure)

"""fig.update_layout(
    title_font_family = 'Courier New'
)"""

app.layout = html.Div(style={'backgroundColor': colors['background']}, children=[
    html.H4(
        children = 'IOON',
        style = {
            'textAlign': 'center',
            'color': colors['text_hola']
        }
    ),
    html.Div([html.Img(id='ioon_image', src='data:image/png;base64,{}'.format(encoded_image_1.decode()),style={
                'height': '15%',
                'width': '15%'
            })
    ], style={'textAlign': 'center'}),
    
    html.Div([
    html.Div([
        html.Div([
            html.H3('Video', style = {'textAlign': 'center', 'color': colors['text_hola']}), # CHAPUZA PARA CUADRAR EL VIDEO EN EL CENTRO
            dash_player.DashPlayer(id ='video', controls = True, volume = 0, url='/Users/sergionavasgarcia/IOON/AENA/DASHBOARD/Turnaround_AENA.mp4',style={
                'height': '10%',
                'width': '10%'
            })
        ],style={'textAlign': 'center'}, className="six columns"),

        html.Div([
           # html.H3('Column 2'),
            dcc.Graph(id='graph', figure=fig),
            dcc.Interval(id='interval-component', interval=1*1000, n_intervals=0)
        ], className="six columns"),
    ], className="row")
]),

    html.Div([html.Button('Start', id='start', n_clicks = 0)], style={'text-align': 'center'}),

    html.Div(html.H5(
        children = 'IOON',
        style = {
            'textAlign': 'center',
            'color': colors['text_hola']
        }
    )),
    
])

@app.callback(Output('graph', 'figure'),
            [Input('interval-component', 'n_intervals'),
            Input('start', 'n_clicks')])

def updateGraph (n, btn):

    global n_abs
    global init

    print(n)
    print(btn)
    print(n_abs)

    if btn == 1 and init !=0 :
        n_abs = n
        init = 0
    

    if btn == 1 and (n-n_abs) < 8:

        fig = dict({
        'data': [
            {'x': [0], 'y': ['Avion parado'], 'type': 'bar', 'name': 'Avion parado', 'orientation': 'h', 'base': 5, 'width': 0.1},
            {'x': [0], 'y': ['Desembarco'], 'type': 'bar', 'name': 'Desembarco', 'orientation': 'h', 'width': 0.1},
            {'x': [0], 'y': ['Maletas'], 'type': 'bar', 'name': 'Maletas', 'orientation': 'h', 'width': 0.1},
            {'x': [0], 'y': ['Repostaje'], 'type': 'bar', 'name': 'Repostaje', 'orientation': 'h', 'width': 0.1},
        ],
        'layout': {
            'title': 'Turnaorund events',
            'plot_bgcolor': colors['background'],
            'paper_bgcolor': colors['background'],
            'font':{
                'color': colors['plot_title'],
            }
        }
        })

    elif btn == 1 and (n-n_abs) < 12: #avion parado

        fig = dict({
        'data': [
            {'x': [n-n_abs-8], 'y': ['Avion parado'], 'type': 'bar', 'name': 'Avion parado', 'orientation': 'h', 'base': 8, 'width': 0.1},
            {'x': [0], 'y': ['Desembarco'], 'type': 'bar', 'name': 'Desembarco', 'orientation': 'h', 'width': 0.1},
            {'x': [0], 'y': ['Maletas'], 'type': 'bar', 'name': 'Maletas', 'orientation': 'h', 'base': 10, 'width': 0.1},
            {'x': [0], 'y': ['Repostaje'], 'type': 'bar', 'name': 'Repostaje', 'orientation': 'h', 'width': 0.1},
        ],
        'layout': {
            'title': 'Turnaorund events',
            'plot_bgcolor': colors['background'],
            'paper_bgcolor': colors['background'],
            'font':{
                'color': colors['plot_title'],
            }
        }
        })

    elif btn == 1 and (n-n_abs) < 14: #congelamos;inicio maletas

        fig = dict({
        'data': [
            {'x': [3], 'y': ['Avion parado'], 'type': 'bar', 'name': 'Avion parado', 'orientation': 'h', 'base': 8, 'width': 0.1},
            {'x': [0], 'y': ['Desembarco'], 'type': 'bar', 'name': 'Desembarco', 'orientation': 'h', 'width': 0.1},
            {'x': [0], 'y': ['Maletas'], 'type': 'bar', 'name': 'Maletas', 'orientation': 'h', 'base': 0, 'width': 0.1},
            {'x': [0], 'y': ['Repostaje'], 'type': 'bar', 'name': 'Repostaje', 'orientation': 'h', 'width': 0.1},
        ],
        'layout': {
            'title': 'Turnaorund events',
            'plot_bgcolor': colors['background'],
            'paper_bgcolor': colors['background'],
            'font':{
                'color': colors['plot_title'],
            }
        }
        })

    elif btn == 1 and (n-n_abs) < 20: #avion parado + maletas

        fig = dict({
        'data': [
            {'x': [n-n_abs-10], 'y': ['Avion parado'], 'type': 'bar', 'name': 'Avion parado', 'orientation': 'h', 'base': 8, 'width': 0.1},
            {'x': [0], 'y': ['Desembarco'], 'type': 'bar', 'name': 'Desembarco', 'orientation': 'h', 'width': 0.1},
            {'x': [n-n_abs-13], 'y': ['Maletas'], 'type': 'bar', 'name': 'Maletas', 'orientation': 'h', 'base': 11, 'width': 0.1},
            {'x': [0], 'y': ['Repostaje'], 'type': 'bar', 'name': 'Repostaje', 'orientation': 'h', 'width': 0.1},
        ],
        'layout': {
            'title': 'Turnaorund events',
            'plot_bgcolor': colors['background'],
            'paper_bgcolor': colors['background'],
            'font':{
                'color': colors['plot_title'],
            }
        }
        })

    elif btn == 1 and (n-n_abs) < 23: #se congela; inicio desembarco

        fig = dict({
        'data': [
            {'x': [10], 'y': ['Avion parado'], 'type': 'bar', 'name': 'Avion parado', 'orientation': 'h', 'base': 8, 'width': 0.1},
            {'x': [0], 'y': ['Desembarco'], 'type': 'bar', 'name': 'Desembarco', 'orientation': 'h', 'base': 0, 'width': 0.1},
            {'x': [7], 'y': ['Maletas'], 'type': 'bar', 'name': 'Maletas', 'orientation': 'h', 'base': 11, 'width': 0.1},
            {'x': [0], 'y': ['Repostaje'], 'type': 'bar', 'name': 'Repostaje', 'orientation': 'h', 'width': 0.1},
        ],
        'layout': {
            'title': 'Turnaorund events',
            'plot_bgcolor': colors['background'],
            'paper_bgcolor': colors['background'],
            'font':{
                'color': colors['plot_title'],
            }
        }
        })

    elif btn == 1 and (n-n_abs) < 25:

        fig = dict({
        'data': [
            {'x': [n-n_abs-12], 'y': ['Avion parado'], 'type': 'bar', 'name': 'Avion parado', 'orientation': 'h', 'base': 8, 'width': 0.1},
            {'x': [n-n_abs-22], 'y': ['Desembarco'], 'type': 'bar', 'name': 'Desembarco', 'orientation': 'h', 'base': 18, 'width': 0.1},
            {'x': [n-n_abs-15], 'y': ['Maletas'], 'type': 'bar', 'name': 'Maletas', 'orientation': 'h', 'base': 11, 'width': 0.1},
            {'x': [0], 'y': ['Repostaje'], 'type': 'bar', 'name': 'Repostaje', 'orientation': 'h', 'base':0, 'width': 0.1},
        ],
        'layout': {
            'title': 'Turnaorund events',
            'plot_bgcolor': colors['background'],
            'paper_bgcolor': colors['background'],
            'font':{
                'color': colors['plot_title'],
            }
        }
        })


    elif btn == 1 and (n-n_abs) < 27:

        fig = dict({
        'data': [
            {'x': [12], 'y': ['Avion parado'], 'type': 'bar', 'name': 'Avion parado', 'orientation': 'h', 'base': 8, 'width': 0.1},
            {'x': [2], 'y': ['Desembarco'], 'type': 'bar', 'name': 'Desembarco', 'orientation': 'h', 'base': 18, 'width': 0.1},
            {'x': [9], 'y': ['Maletas'], 'type': 'bar', 'name': 'Maletas', 'orientation': 'h', 'base': 11, 'width': 0.1},
            {'x': [0], 'y': ['Repostaje'], 'type': 'bar', 'name': 'Repostaje', 'orientation': 'h', 'base':0, 'width': 0.1},
        ],
        'layout': {
            'title': 'Turnaorund events',
            'plot_bgcolor': colors['background'],
            'paper_bgcolor': colors['background'],
            'font':{
                'color': colors['plot_title'],
            }
        }
        })        
   
    elif btn == 1 and (n-n_abs) < 35:

        fig = dict({
        'data': [
            {'x': [n-n_abs-14], 'y': ['Avion parado'], 'type': 'bar', 'name': 'Avion parado', 'orientation': 'h', 'base': 8, 'width': 0.1},
            {'x': [n-n_abs-24], 'y': ['Desembarco'], 'type': 'bar', 'name': 'Desembarco', 'orientation': 'h', 'base': 18, 'width': 0.1},
            {'x': [n-n_abs-17], 'y': ['Maletas'], 'type': 'bar', 'name': 'Maletas', 'orientation': 'h', 'base': 11, 'width': 0.1},
            {'x': [n-n_abs-26], 'y': ['Repostaje'], 'type': 'bar', 'name': 'Repostaje', 'orientation': 'h', 'base':20, 'width': 0.1},
        ],
        'layout': {
            'title': 'Turnaorund events',
            'plot_bgcolor': colors['background'],
            'paper_bgcolor': colors['background'],
            'font':{
                'color': colors['plot_title'],
            }
        }
        })

    elif btn == 1 and (n-n_abs) < 37:

        fig = dict({
        'data': [
            {'x': [20], 'y': ['Avion parado'], 'type': 'bar', 'name': 'Avion parado', 'orientation': 'h', 'base': 8, 'width': 0.1},
            {'x': [10], 'y': ['Desembarco'], 'type': 'bar', 'name': 'Desembarco', 'orientation': 'h', 'base': 18, 'width': 0.1},
            {'x': [17], 'y': ['Maletas'], 'type': 'bar', 'name': 'Maletas', 'orientation': 'h', 'base': 11, 'width': 0.1},
            {'x': [8], 'y': ['Repostaje'], 'type': 'bar', 'name': 'Repostaje', 'orientation': 'h', 'base':20, 'width': 0.1},
        ],
        'layout': {
            'title': 'Turnaorund events',
            'plot_bgcolor': colors['background'],
            'paper_bgcolor': colors['background'],
            'font':{
                'color': colors['plot_title'],
            }
        }
        })
    
    elif btn == 1 and (n-n_abs) < 1000:

        fig = dict({
        'data': [
            {'x': [n-n_abs-16], 'y': ['Avion parado'], 'type': 'bar', 'name': 'Avion parado', 'orientation': 'h', 'base': 8, 'width': 0.1},
            {'x': [10], 'y': ['Desembarco'], 'type': 'bar', 'name': 'Desembarco', 'orientation': 'h', 'base': 18, 'width': 0.1},
            {'x': [n-n_abs-19], 'y': ['Maletas'], 'type': 'bar', 'name': 'Maletas', 'orientation': 'h', 'base': 11, 'width': 0.1},
            {'x': [n-n_abs-28], 'y': ['Repostaje'], 'type': 'bar', 'name': 'Repostaje', 'orientation': 'h', 'base':20, 'width': 0.1},
        ],
        'layout': {
            'title': 'Turnaorund events',
            'plot_bgcolor': colors['background'],
            'paper_bgcolor': colors['background'],
            'font':{
                'color': colors['plot_title'],
            }
        }
        }) 
    else:

        fig = dict({
        'data': [
            {'x': [0], 'y': ['Avion parado'], 'type': 'bar', 'name': 'Avion parado', 'orientation': 'h', 'base': 0.5, 'width': 0.1},
            {'x': [0], 'y': ['Desembarco'], 'type': 'bar', 'name': 'Desembarco', 'orientation': 'h', 'base': 2, 'width': 0.1},
            {'x': [0], 'y': ['Maletas'], 'type': 'bar', 'name': 'Maletas', 'orientation': 'h', 'width': 0.1},
            {'x': [0], 'y': ['Repostaje'], 'type': 'bar', 'name': 'Repostaje', 'orientation': 'h', 'width': 0.1},
        ],
        'layout': {
            'title': 'Turnaorund events',
            'plot_bgcolor': colors['background'],
            'paper_bgcolor': colors['background'],
            'font':{
                'color': colors['plot_title'],
            }

        }
        })

    return fig

@app.callback(Output('video', 'playing'),
            Input('start', 'n_clicks'))
def updateVideo (btn):
    if btn == 1:
        return True
    else:
        return False

server = app.server

@server.route('/Users/sergionavasgarcia/IOON/AENA/DASHBOARD/<path:path>')
def serve_static(path):
    root_dir = os.getcwd()
    return flask.send_from_directory(os.path.join(root_dir, '/Users/sergionavasgarcia/IOON/AENA/DASHBOARD/'), path)

if __name__ == '__main__':
    app.run_server(debug=True)